### Variables
#
# Logo key. Use Mod1 for Alt.
set $alt Mod1
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred terminal emulator
set $term alacritty
# Your preferred application launcher
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.
set $menu dmenu_path | dmenu | xargs swaymsg exec --

# define styles for windows
default_border none
default_floating_border normal

# font for window titles. ISO 10646 = Unicode
font pango:Hack, DejaVu Sans Mono 10

# set long command to float, sticky and resize windows
set $float_video floating enable, border none, resize shrink width 10000 px, resize grow width 885 px, resize shrink height 10000 px, resize grow height 490 px, sticky toggle, exec --no-startup-id movefloat up right

# use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod


### Output configuration
#
# Default wallpaper (more resolutions are available in /usr/share/backgrounds/sway/)
output HDMI-A-1 pos 0 0 res 1920x1080
output eDP-1 pos 1920 0 res 1920x1080
output * bg ~/img/wallpaper fill
#
# Example configuration:
#
#   output HDMI-A-1 resolution 1920x1080 position 1920,0
#
# You can get the names of your outputs by running: swaymsg -t get_outputs

### Idle configuration
#
# Example configuration:
#
# exec swayidle -w \
#          timeout 300 'swaylock -f -c 000000' \
#          timeout 600 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
#          before-sleep 'swaylock -f -c 000000'
#
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.

### Input configuration
#
# Example configuration:
#
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.
input * {
    xkb_layout br
    xkb_variant abnt2
    xkb_options ctrl:swapcaps
}

### Key bindings
#
# Basics:
#
    # Start a terminal
    bindsym $mod+Return exec $term

    # Kill focused window
    bindsym $mod+q kill

    # Start your launcher
    bindsym $mod+d exec $menu

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # Reload the configuration file
    bindsym $mod+Shift+r reload

    # Exit instantly
    bindsym $mod+Shift+period exit

    # Exit sway (logs you out of your Wayland session)
    bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'
#
# Moving around:
#
    # Move your focus around
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right
    # Or use $mod+[up|down|left|right]
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # Move the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    # Ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right
#
# Workspaces:
#
    # switch to workspace
    bindsym $mod+1 workspace number 1
    bindsym $mod+2 workspace number 2
    bindsym $mod+3 workspace number 3
    bindsym $mod+4 workspace number 4
    bindsym $mod+5 workspace number 5
    bindsym $mod+6 workspace number 6
    bindsym $mod+7 workspace number 7
    bindsym $mod+8 workspace number 8
    bindsym $mod+9 workspace number 9
    bindsym $mod+0 workspace number 10

    bindsym $mod+Tab workspace next
    bindsym $mod+Shift+Tab workspace prev

    # move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace number 1
    bindsym $mod+Shift+2 move container to workspace number 2
    bindsym $mod+Shift+3 move container to workspace number 3
    bindsym $mod+Shift+4 move container to workspace number 4
    bindsym $mod+Shift+5 move container to workspace number 5
    bindsym $mod+Shift+6 move container to workspace number 6
    bindsym $mod+Shift+7 move container to workspace number 7
    bindsym $mod+Shift+8 move container to workspace number 8
    bindsym $mod+Shift+9 move container to workspace number 9
    bindsym $mod+Shift+0 move container to workspace number 10
#
# Layout stuff:

    # Switch the current container between different layout styles
    bindsym $mod+s layout stacking
    bindsym $mod+w layout tabbed
    bindsym $mod+e layout toggle splitS

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+space floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+space focus mode_toggle

    # Move focus to the parent container
    bindsym $mod+a focus parent

    # toggle stickiness (workspace independence)
    bindsym $mod+Ctrl+t sticky toggle

    # toggle tiling / floating
    bindsym $mod+Shift+apostrophe floating enable; border toggle
    bindsym $mod+apostrophe floating disable; border toggle

#
# Keyboard shortcuts
#
    bindsym $mod+F1 exec i3-sensible-terminal
    bindsym $mod+F5 exec wezterm
    bindsym $mod+F2 exec --no-startup-id sensible-browser
    bindsym $mod+F3 exec --no-startup-id telegram-desktop
    bindsym $mod+F4 exec --no-startup-id pavucontrol
    bindsym $mod+F8 exec --no-startup-id screenlayout
    bindsym $mod+Shift+F8 exec --no-startup-id screenlayout default.sh
    bindsym $mod+F10 exec --no-startup-id $terminal -name khaki

    bindsym $mod+Shift+Return exec --no-startup-id togglekbd

    # Calcurse o/
    bindsym $mod+Shift+c exec --no-startup-id wezterm start --class calcurse -- calcurse -q 2>>~/.xsession-errors

    bindsym $mod+Shift+t exec --no-startup-id runtwitch
    bindsym $mod+y exec --no-startup-id loadvideo -c

    # notifications
    bindsym $mod+t exec --no-startup-id showtime
    bindsym $mod+b exec --no-startup-id battstatus
    bindsym $mod+c exec --no-startup-id calendar

    # lock
    bindsym $mod+$alt+l exec --no-startup-id swaylock -i ~/img/lock

    # screenshots/recording
    bindsym $mod+shift+s exec --no-startup-id grimshot copy area
    bindsym --release $mod+shift+v exec --no-startup-id wf-recorder -g "$(slurp)"

    # Pulse Audio controls
    bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +2%
    bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -2%
    bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle

#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # Ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $mod+r mode "default"
}
bindsym $mod+r mode "resize"

#
# Status Bar:
#
# Read `man 5 sway-bar` for more information about this section.
bar {
    # When the status_command prints a new line to stdout, swaybar updates.
    # The default just shows the current date and time.
    status_command i3status-rs

    font pango:FontAwesome, Fira Code 12

    colors {
        # class            border  backgr. text
        focused_workspace  #333333 #333333 #ffffff
        active_workspace   #0F0F0F #0F0F0F #ffffff
        inactive_workspace #0F0F0F #0F0F0F #AAAAAA
        urgent_workspace   #800000 #800000 #ffffff

        separator #ffffff
        statusline #cccccc
        background #000000
        focused_separator #cccccc
    }
}

# colors
# class                 border  backgr. text    indicator
client.focused          #525252 #333333 #CCCCCC #CCCCCC
client.focused_inactive #333333 #0F0F0F #AAAAAA #666666
client.unfocused        #000000 #000000 #AAAAAA
client.urgent           #900000 #900000 #CCCCCC

# workspace assignments
for_window [class="^[Cc]hromium"] move to workspace number 2
for_window [class="^[Ff]irefox"] move to workspace number 2
for_window [class="^[Gg]oogle"] move to workspace number 2
for_window [class="^[Tt]elegram"] move to workspace number 1
for_window [instance="khaki" class="URxvt"] move to workspace number 10

# window styling
for_window [class="^[Cc]hromium"] border none
for_window [class="^[Gg]oogle"] border none
for_window [class="^[Mm]pv"] $float_video
for_window [class="^[Pp]avucontrol"] floating enable border normal
for_window [class="^[Tt]elegram"] border none
for_window [class="league of legends.exe"] floating disabled border none
for_window [instance="urxvt-console"] floating enable
for_window [class="[Cc]alcurse" instance="[Cc]alcurse"] $float_video

# quality of life (no focus on new windows, just set them red)
focus_on_window_activation urgent
no_focus [class=".*"]

exec --no-startup-id bash /home/jptiz/.profile
exec --no-startup-id /home/jptiz/.local/bin/setupkeys
exec --no-startup-id bash $home/.screenlayout/default.sh
# exec --no-startup-id hsetroot -cover $home/img/wallpaper
exec --no-startup-id compton -b
exec --no-startup-id redshift

include /etc/sway/config.d/*
include config.d/*
