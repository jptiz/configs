function add-path {
    [[ ":$PATH:" != *":${1}:"* ]] && export PATH="${1}:${PATH}"
}

function first-of {
    for i in "$@"
    do
        if /usr/bin/which "$i" 2> /dev/null > /dev/null
        then
            echo "$i"
            return
        fi
    done
}

export GOPATH="$HOME/.gopath"

add-path /usr/local/bin
add-path "${GOPATH}/bin"
add-path "${HOME}"/.local/bin
add-path "${DEVKITARM}"/bin
add-path ~/.cargo/bin
add-path ~/pkg/scripts/bin
add-path ~/.git-configs/files/bin
add-path ~/.npm-global/bin

export BGCMD="hsetroot -full"
export BROWSER="firefox"
export CFLAGS="-Werror=implicit-function-declaration"
export CMAKE_GENERATOR="Ninja"
export COMPMGR="picom"
export EDITOR="$(first-of nvim vim nano)"
export JUNIT_HOME=/usr/share/java
export TERMINAL="$(first-of wezterm alacritty urxvt)"
export TEXINPUTS="::${HOME}/pkg/tex/"
export VPROJECT_TEMPLATES="${HOME}/.config/v-project/templates/"

# Depend on previous variables
export CLASSPATH=".:src:${JUNIT_HOME}/junit.jar"

# To cap games at 60FPS
export MANGOHUD=1
