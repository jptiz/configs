# reload config file (change file location to your the tmux.conf you want to use)
bind r source-file ~/.config/tmux/config \; display '~/.tmux.conf sourced'

# 0 is too far from ` ;)
set -g base-index 1

# Navigate through windows
bind-key C-u previous-window
bind-key C-i next-window

# Automatically set window title
set-window-option -g automatic-rename on
set-option -g set-titles on

# faster command sequences
set -s escape-time 10

set-option -g prefix C-b
bind C-b send-prefix

# Disable mouse mode (tmux 2.1 and above)
set -g mouse on

# Create split panes
bind v split-window -h
bind s split-window -v

## switch panes using Alt-arrow without prefix
bind h select-pane -L
bind l select-pane -R
bind k select-pane -U
bind j select-pane -D

# ------------------------------------------------------------------------------
# #design
set -g default-terminal "screen-256color"

# loud or quiet? -- what
set -g visual-activity off
set -g visual-bell off
set -g visual-silence off
setw -g monitor-activity off
set -g bell-action none

#  modes
setw -g clock-mode-colour colour5
setw -g mode-style 'fg=colour1 bg=colour230 bold'

# panes
set -g pane-border-style 'fg=colour19 bg=colour0'
set -g pane-active-border-style 'bg=colour0 fg=colour9'

# statusbar
set -g status-position top
set -g status-justify centre
set -g status-style 'bg=colour0 fg=colour137 dim'
set -g status-left '#[fg=colour233,bg=colour8] tmux (#S) #[fg=colour8,bg=colour0]'
set -g status-right '#[fg=colour8,bg=colour0]#[fg=colour233,bg=colour8]:) '
set -g status-right-length 50
set -g status-left-length 50

setw -g window-status-current-style 'fg=colour1 bg=colour0 bold'
setw -g window-status-current-format ' #I#[fg=colour249]:#[fg=colour255]#W#[fg=colour249] '

setw -g window-status-style 'fg=colour9 bg=colour0'
setw -g window-status-format '  #I#[fg=colour237]:#[fg=colour250]#W#[fg=colour244]  '

setw -g window-status-bell-style 'fg=colour255 bg=colour1 bold'

# messages
set -g message-style 'fg=colour249 bg=colour241 bold'
