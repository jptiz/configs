-- Python virtualenv paths
vim.g.python3_host_prog = string.format(vim.env.HOME .. '/.local/nvenv/nvim3/bin/python')
vim.g.loaded_ruby_provider = 0
vim.g.loaded_perl_provider = 0
vim.g.loaded_node_provider = 0

-- General customizations

-- Theme
if not string.find(vim.env['TERM'], 'linux') then
    vim.opt.termguicolors = true
    vim.cmd('colorscheme zenburn')
    --  remove bg color (use default one, normally transparent for me)
    vim.api.nvim_set_hl(0, 'Normal', { ctermfg = 188, ctermbg = 'None' })
    -- hi Normal ctermfg=188 ctermbg=None guibg=None
    --  highlight current line
    vim.opt.cursorline = true
    vim.g.airline_powerline_fonts=1 --  use powerline arrows
    vim.g.airline_theme='zenburn'   --  nicer theme
else
    vim.cmd('colorscheme desert')
end


--  clear whitespace before saving
vim.api.nvim_create_autocmd(
  { "BufWritePre" },
  {
    pattern = '*',
    command = ':%s/\\s\\+$//e',
    group = vim.api.nvim_create_augroup('ClearWhitespace', {}),
  }
)

vim.api.nvim_create_autocmd(
  { "TermOpen" },
  {
    pattern = '*',
    command = 'setlocal nonumber norelativenumber',
    group = vim.api.nvim_create_augroup('terminal', { clear = true }),
  }
)

--  highlight stuff after column 80
vim.fn['match']('ErrorMsg', "\\%81v.\\+")
