local telescope_builtin = require('telescope.builtin')

local default_keymaps = {
  [{ '', 'n', 'i', 'v' }] = {
    -- Disable arrows
    { '<Left>', '<NOP>', { silent = true, noremap = true },
      'Arrow keys are disabled, use hjkl'
    },
    { '<Down>', '<NOP>', { silent = true, noremap = true } },
    { '<Up>', '<NOP>', { silent = true, noremap = true } },
    { '<Right>', '<NOP>', { silent = true, noremap = true } },
  },
  n = {
    { 'q', ':q<CR>', { silent = true, },
      'Quit with q when no changes were made',
    },
    { '<leader>i', 'gg=G\'\'', { silent = true, },
      'Indent the whole file',
    },
    { '<leader>h', ':nohl<CR>', { silent = true, },
      'Remove highlights',
    },
    { '<F11>', ':set list!<CR>', { silent = true, },
      'Toggle showing whitespace as characters',
    },
    { '<F12>', ':set rnu!<CR>', { silent = true, },
      'Toggle relative line numbers',
    },
    { '<C-s>', ':w<CR>', { silent = true, noremap = true, },
      'Quick save',
    },
    { '<C-u>', 'gT', { silent = true, noremap = true, },
      'Previous tab',
    },
    { '<C-i>', 'gt', { silent = true, noremap = true, },
      'Next tab',
    },
    { 'go', vim.diagnostic.open_float, { noremap = true, silent = true },
      'Open diagnostics float window for current line',
    },
    { '[d', vim.diagnostic.goto_prev, { noremap = true, silent = true },
      'Go to previous diagnostic' },
    { ']d', vim.diagnostic.goto_next, { noremap = true, silent = true },
      'Go to next diagnostic' },
    { '<leader>p', telescope_builtin.find_files, {} },
    { '<leader>c', telescope_builtin.commands, {} },
  },
  i = {
    { '<F1>', '<NOP>', { silent = true }, 'Disable F1 calling help' },
    { '<F11>', '<C-o><F11>', { silent = true }, 'Toggle showing whitespace as characters' },
    { '<F12>', '<C-o><F12>', { silent = true }, 'Toggle relative line numbers' },
  },
  v = {
    { '<leader>nn', ':NR<CR><C-w>_', { silent = true },
      'Narrow to region and maximize narrowed window',
    },
  },
  c = {
    { 'w!!', 'execute \'silent! write !sudo tee % > /dev/null\' <bar> edit!<CR>',
      { silent = false, noremap = true, },
      'Save file as sudo (when you forgot to open it as so)',
    },
  },
}

function Map_keys(keymaps)
  for mode, mappings in pairs(keymaps) do
    for _, mapping in pairs(mappings) do
      local key, map, opts, desc = unpack(mapping)
      vim.keymap.set(mode, key, map, { desc = desc, unpack(opts) })
    end
  end
end

Map_keys(default_keymaps)
