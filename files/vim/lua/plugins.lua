Plug = vim.fn['plug#']

-- Vundle setup
vim.call('plug#begin')

--------------------------------------------------------------------------------
-- General

-- Code snippets
Plug 'garbas/vim-snipmate'

-- Airline bar and etc.
Plug 'bling/vim-airline'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'tomtom/tlib_vim'
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'chrisbra/NrrwRgn'
Plug 'osyo-manga/vim-over'

-- Handy utils
  -- for automatic tables organizing
  -- (:Tab /<token>)
  Plug 'godlygeek/tabular'
-- Git utils
  -- Provides :Git commands
  Plug 'tpope/vim-fugitive'

  -- For conflict markers visualization & resolution
  Plug('akinsho/git-conflict.nvim', { version = '*' })

  -- Just a few markers for quick references
  Plug('lewis6991/gitsigns.nvim')

-- Fuzzy finder
  -- (Optionals' dependencies)
  Plug 'nvim-lua/popup.nvim'

  -- (Dependencies)
  Plug 'nvim-lua/plenary.nvim'

  -- Telescope
  Plug('nvim-telescope/telescope.nvim', { tag = '0.1.5' })

  -- Optional: Telescope extensions
  Plug 'nvim-telescope/telescope-dap.nvim'
  Plug 'nvim-telescope/telescope-media-files.nvim'

  -- Neotest
  Plug 'nvim-lua/plenary.nvim'
  Plug 'antoinemadec/FixCursorHold.nvim'
  Plug('nvim-treesitter/nvim-treesitter', { ['do'] = vim.fn['TSUpdate'] })
  Plug 'nvim-neotest/neotest'
  -- Plug 'nvim-treesitter/nvim-treesitter'

  -- Optional: icons in picker
  Plug 'nvim-tree/nvim-web-devicons'

--------------------------------------------------------------------------------
-- Completion sources for ncm2

-- LSP
  -- Server installation manager
  Plug 'williamboman/mason.nvim'
  Plug 'williamboman/mason-lspconfig.nvim'
  -- Configuration manager
  Plug 'neovim/nvim-lspconfig'

-- nvim-cmp:
  -- nvim-lsp integration
  Plug 'hrsh7th/cmp-nvim-lsp'

  -- Unsure
  Plug 'hrsh7th/cmp-buffer'
  Plug 'hrsh7th/cmp-path'
  Plug 'hrsh7th/cmp-cmdline'
  Plug 'hrsh7th/nvim-cmp'

  -- For vsnip users.
  Plug 'hrsh7th/cmp-vsnip'
  Plug 'hrsh7th/vim-vsnip'

-- Debugging
  Plug 'mfussenegger/nvim-dap'
  Plug 'rcarriga/nvim-dap-ui'
  Plug 'mfussenegger/nvim-dap-python'

-- Testing

  -- Neotest adapters
  Plug 'nvim-neotest/neotest-python'

  -- JavaScript
  Plug 'David-Kunz/jester'

  -- Coverage
  Plug 'andythigpen/nvim-coverage'

--------------------------------------------------------------------------------
-- Visual

-- Theme
Plug 'jnurmine/Zenburn'
Plug 'vim-airline/vim-airline-themes'  -- zenburn for airline

-- Syntax Highlighting only
Plug 'cespare/vim-toml'

vim.call('plug#end')
