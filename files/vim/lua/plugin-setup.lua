--  Dependencies
require("nvim-treesitter.configs").setup {
  ensure_installed = { 'c', 'cpp', 'python', 'javascript' },
}
--  Language servers
local mason = require('mason')
local mason_lspconfig = require('mason-lspconfig')
local cmp = require('cmp')

cmp.setup({
  snippet = {
    expand = function(args)
      vim.fn['vsnip#anonymous'](args.body)
    end
  },
  mapping = {
    ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
    ['<CR>'] = cmp.mapping.confirm({ select = true }),
  },
  sources = cmp.config.sources({
    { name = 'lsp_config' },
    { name = 'vsnip' },
  }, {
    { name = 'buffer' },
  })
})

vim.diagnostic.config({
  virtual_text = {
    source = 'always',
  },
  float = {
    source = 'always',
  },
})

mason.setup {
  ui = {
    border = 'shadow',
  }
}

-- Automatically reload after `:LspInstall <server>` so we don't have to
-- restart neovim
mason.post_install_hook = function()
  -- triggers the FileType autocmd that starts the server
  vim.cmd('bufdo e')
end

local opts = {
  capabilities = require('cmp_nvim_lsp').default_capabilities(),
  on_attach = function(_, buff_number)
    local opts = { noremap = true, silent = true, buffer = buff_number }

    Map_keys({
      n = {
        { 'gr', vim.lsp.buf.references, opts,
          'Find references to object under cursor' },
        { 'gD', vim.lsp.buf.declaration, opts,
          'Go to object declaration' },
        { 'gd', vim.lsp.buf.definition, opts,
          'Go to object definition' },
        { '<leader>k', vim.lsp.buf.hover, opts,
          'Display info about object' },
        { '<leader>rn', vim.lsp.buf.rename, opts,
          'Renames object' },
        { '<leader>a', vim.lsp.buf.code_action, opts,
          'Execute code action' },
        { '<leader>f', '<cmd>lua vim.lsp.buf.format({async = true})<CR>', opts,
          'Formats current file' },
        { 'gi', vim.lsp.buf.implementation, opts,
          'Go to implementation' },
      },
      i = {
        { '<C-k>', vim.lsp.buf.signature_help, opts,
          'Gets current function\'s signature' },
      },
    })
  end,
}

local lsp_config = require('lspconfig')

mason_lspconfig.setup {
  ensure_installed = { 'pyright' },
}
mason_lspconfig.setup_handlers {
  function (server_name)
    lsp_config[server_name].setup(opts)
  end,
  ['clangd'] = function ()
    -- opts.cmd = {
    --   'clangd', '--background-index', '--clang-tidy',
    -- }
    lsp_config.clangd.setup(opts)
  end,
  ['pylsp'] = function ()
    lsp_config.pylsp.setup {
      settings = {
        pylsp = {
          plugins = {
            -- Enabled:
            black = { enabled = true },
            flake8 = { enabled = true },
            mypy = { enabled = true },
            -- Disabled:
            pylint = { enabled = false },
            pycodestyle = { enabled = false },
          },
        },
      },
    }
  end,
  ['texlab'] = function()
    opts.settings = {
      texlab = {
        build = {
          args = {'-X', 'compile', '%f', '--synctex', '--keep-logs', '--keep-intermediates', '--outdir', 'build'},
          executable = 'tectonic',
          forwardSearchAfter = true,
          onSave = true,
        },
        auxDirectory = 'build',
        bibtexFormatter = 'texlab'
      },
      chktex = {
        onEdit = true,
        onOpenAndSave = true,
      }
    }
    lsp_config.texlab.setup(opts)
  end,
  ['lua_ls'] = function()
    lsp_config.lua_ls.setup({
      unpack(opts),
      settings = {
        Lua = {
          runtime = {
            version = "LuaJIT",
          },
          diagnostics = {
            globals = { 'vim', },
          },
          telemetry = { enable = false },
          hint = { enable = true },
          workspace = {
            library = {
              [vim.fn.expand "$VIMRUNTIME/lua"] = true,
              [vim.fn.expand "$VIMRUNTIME/lua/vim/lsp"] = true,
            }
          }
        }
      }
    })
  end,
  -- ['qml_lsp'] = function ()
  --   lsp_config.qml_lsp.setup(opts)
  -- end,
}

-- ----------------------------------------------------------------------------
-- Other plugins
-- ----------------------------------------------------------------------------
require('debugger')

-- ----------------------------------------------------------------------------
local telescope = require('telescope')
telescope.load_extension('dap')
telescope.load_extension('media_files')

-- Testing plugins
require('dap-python').setup('~/.local/share/nvim/mason/packages/debugpy/venv/bin/python')
require('jester').setup({
  dap = {
    console = 'externalTerminal'
  }
})

neotest = require("neotest")
neotest.setup({
  adapters = {
    require("neotest-python")({
      args = {'-vv', '-rP'},
    }),
  }
})

require('coverage').setup()

require('git-conflict').setup()
require('gitsigns').setup()

-- ============================================================================

--  Snip-Mate
vim.g.snipMate = { snippet_version = 1 }

--  Netrw setup
vim.g.netrw_banner = 0
vim.g.netrw_browse_split = 2
vim.g.netrw_altv = 1
vim.g.netrw_liststyle = 3
vim.g.netrw_preview = 1

--  Airline setup
vim.g['airline#extensions#tabline#enabled'] = 1       -- enable tabline upport
vim.g['airline#extensions#tabline#tab_nr_type'] = 1   -- show tab numbers
vim.g['airline#extensions#tabline#show_buffers'] = 0  -- hide buffers
vim.g['airline#extensions#tabline#tab_min_count'] = 2 -- hide tabline for one tab

--  Rust configs
vim.g.rustfmt_autosave = 1
