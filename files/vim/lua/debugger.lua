dap = require("dap")
dapui = require("dapui")

dap.adapters.cppdbg = {
  id = 'cppdbg',
  type = 'executable',
  command = '/home/jptiz/pkg/cpptools-linux/extension/debugAdapters/bin/OpenDebugAD7',
}

dap.configurations.cpp = {
    {
        name = "Launch",
        type = "cppdbg",
        request = "launch",
        program = function()
            return vim.fn.input("Executable: ", '/home/jptiz/w/1/envs/alfasim_gui-py36/bin/python', 'file_in_path')
        end,
        cwd = "${workspaceFolder}",
        stopOnEntry = false,
        args = {'-m', 'pytest', 'source/python/alfasim_calc/solver/_tests/test_solver_with_ph_pvt_model.py::test_ph_heat_exchange_model', '-svv'},
        runInTerminal = true,
    },
    {
        name = 'Attach to gdbserver :1234',
        type = 'cppdbg',
        request = 'launch',
        MIMode = 'gdb',
        miDebuggerServerAddress = 'localhost:1234',
        miDebuggerPath = '/home/jptiz/w/1/envs/alfasim_gui-py36/bin/gdb',
        cwd = '${workspaceFolder}',
        program = function()
            return vim.fn.input('Path to executable: ', '', 'file')
        end,
    },
}

dapui.setup()

-- Keybindings
local function map(mode, keys, cmd, opts)
  local options = { noremap = true, silent = true }
  if opts then
    options = vim.tbl_extend('force', options, opts)
  end
  vim.keymap.set(mode, keys, cmd, options)
end
map("n", "<leader>n", dap.step_over, {})
map("n", "<leader>>", dap.step_into, {})
map("n", "<leader>b", dap.toggle_breakpoint, {})

map("n", "<F2>", dapui.toggle, {})
