nmap <silent> <leader>= yypVr=
nmap <silent> <leader>- yypVr-
nmap <silent> <leader>u @='0i#<C-V><ESC>'<CR>

set shiftwidth=2
set tabstop=2

" make index
fu! MakeIndex()
    :1,$d|0r !~/dev/py/vim-tools/md/make_index.py < %
endfunction

command! MakeIndex call MakeIndex()
