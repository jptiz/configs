setlocal cinoptions=g0  " make C++ scope specifiers unindented

"let g:clang_dotfile='.clang_complete'
"let g:clang_diagsopt=''

" extend highlight to column 120
match ErrorMsg '\%121v.\+'
