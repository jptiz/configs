local options = {
  title = true,

  --  Automatically reload files after changes
  autoread = true,

  -- make file search recursive
  path = '**',
  -- enable cool menu
  wildmenu = true,

  -- get mouse working on mouse-capable terminals
  mouse = 'a',

  -- make backspace work everywhere
  backspace = 'indent,eol,start',

  -- always show status in last window
  laststatus = 2,
  -- no delay when going back into normal mode
  ttimeoutlen = 0,
  -- make vim clipboard the same as the system one
  clipboard = 'unnamedplus',
  -- disable default folding of text
  foldenable = false,
  -- no annoying backups of files
  backup = false,
  -- line numbers
  number = true,
  -- show row and column on bottom right
  ruler = true,

  -- spaces instead of tab
  expandtab = true,
  -- tabwidth is 4 spaces
  tabstop = 4,
  -- indent by 4 with >> and <<
  shiftwidth = 4,
  -- start nextline indented
  autoindent = true,

  -- highlight searches
  hlsearch = true,
  -- do incremental searching
  incsearch = true,
  -- jump to matches when entering regexp
  showmatch = true,
  -- ignore case on search
  ignorecase = true,
  -- uppercase forces search casing
  smartcase = true,

  -- no preview window for completion

  splitright = true,
  splitbelow = true,

  listchars = { tab = '——', space = '·' },
}

for k, v in pairs(options) do
  vim.opt[k] = v
end

vim.opt.completeopt:remove { preview }

if vim.fn.has('persistent_undo') then
    local undodir = '~/.vim/undodir'
    vim.call("system", 'mkdir ', undodir)
    vim.opt.undofile = true
end

vim.g.mapleader = ','

require('plugins')
require('plugin-setup')
require('keymaps')
require('custom')
