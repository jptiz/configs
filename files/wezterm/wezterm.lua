local wezterm = require 'wezterm';

local keys_switch_tab_table = {};
for i = 0, 8 do
  table.insert(keys_switch_tab_table, {
    key = tostring(i+1),
    mods = "LEADER",
    action = wezterm.action{ActivateTab=i},
  });
end

return {
  ------------------------------------------------------------------------------
  -- Visual style
  ------------------------------------------------------------------------------
  color_scheme = "Tiz",
  font = wezterm.font_with_fallback({
    -- ->
    "Fira Code",
    "Hack",
  }),
  font_size = 10,
  window_background_opacity = 0.9,
  text_background_opacity = 1.0,
  -- Remember: if scheme gets too big, you can set it into a separated file.
  color_schemes = {
    ["Tiz"] = {
      background = "#3f3f3f",
      ansi = {
        "#3f3f3f",
        "#705050",
        "#60b48a",
        "#dfaf8f",
        "#506070",
        "#dc8cc3",
        "#8cd0d3",
        "#dcdccc",
      },
      brights = {
        "#709080",
        "#dca3a3",
        "#c3bf9f",
        "#f0dfaf",
        "#94bff3",
        "#ec93d3",
        "#93e0e3",
        "#ffffff",
      },
    },
  },
  window_padding = {
    left = 0,
    right = 0,
    top = 0,
    bottom = 0,
  },
  window_decorations = "RESIZE",
  term = "xterm-256color",
  ------------------------------------------------------------------------------
  -- Keybindings
  ------------------------------------------------------------------------------
  disable_default_key_bindings = true,
  leader = { mods = "CTRL", key = "a", },
  keys = {
    {
      mods = "LEADER", key = "r",
      action = "ReloadConfiguration"
    },
    {
      mods = "LEADER", key = "v",
      action = wezterm.action{SplitHorizontal={domain="CurrentPaneDomain"}}
    },
    {
      mods = "LEADER", key = "s",
      action = wezterm.action{SplitVertical={domain="CurrentPaneDomain"}}
    },
    {
      mods = "LEADER", key = "t",
      action = wezterm.action{SpawnTab="CurrentPaneDomain"}
    },
    {
      mods = "LEADER", key = "w",
      action = wezterm.action{CloseCurrentTab={confirm=true}}
    },
    {
      mods = "LEADER", key = "q",
      action = wezterm.action{CloseCurrentPane={confirm=true}}
    },
    {
      mods = "LEADER", key = "h",
      action = wezterm.action{ActivatePaneDirection="Left"}
    },
    {
      mods = "LEADER", key = "j",
      action = wezterm.action{ActivatePaneDirection="Down"}
    },
    {
      mods = "LEADER", key = "k",
      action = wezterm.action{ActivatePaneDirection="Up"}
    },
    {
      mods = "LEADER", key = "l",
      action = wezterm.action{ActivatePaneDirection="Right"}
    },
    {
      mods = "LEADER|SHIFT", key = "L",
      action = "ShowLauncher"
    },
    {
      mods = "CTRL|SHIFT", key = "c",
      action = wezterm.action{CopyTo="Clipboard"}
    },
    {
      mods = "CTRL|SHIFT", key = "v",
      action = wezterm.action{PasteFrom="Clipboard"}
    },
    {
      mods = "SHIFT", key = "PageUp",
      action = wezterm.action{ScrollByPage=-1}
    },
    {
      mods = "SHIFT", key = "PageDown",
      action = wezterm.action{ScrollByPage=1}
    },
    {
      mods = "CTRL|SHIFT", key = "=",
      action = "IncreaseFontSize",
    },
    {
      mods = "LEADER", key = "=",
      action = "IncreaseFontSize",
    },
    {
      mods = "CTRL|SHIFT", key = "-",
      action = "DecreaseFontSize",
    },
    {
      mods = "LEADER", key = "-",
      action = "DecreaseFontSize",
    },
    {
      mods = "CTRL", key = "Tab",
      action = wezterm.action{ActivateTabRelative=1},
    },
    {
      mods = "CTRL|SHIFT", key = "Tab",
      action = wezterm.action{ActivateTabRelative=-1},
    },
    {
      mods = "LEADER", key = "r",
      action = wezterm.action.RotatePanes "Clockwise",
    },
    table.unpack(keys_switch_tab_table),
  },
  ---------------------------------------------------------------------------
  -- Behavioural
  ---------------------------------------------------------------------------
  -- default_prog = {"${SHELL}", "-ic", "tmux", "-v", "source-file", "~/.config/tmux/config"},
  exit_behavior = "Close",
  check_for_updates = false,
  debug_key_events = true,
  launch_menu = {
    {
      label = "Task Manager",
      args = {"htop"},
    },
    {
      label = "Zulip",
      args = {"zulip-term"},
    },
  },
  warn_about_missing_glyphs = false,
}
